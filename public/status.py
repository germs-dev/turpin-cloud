import sys
import pathlib
import socket # for hostname
import os # unmae
import platform

def get_cpu_temp():

  temp_path = "/sys/class/thermal/thermal_zone0/temp"
  p = pathlib.Path(temp_path)

  temp = 0
  if p.exists():
    f = open(temp_path, "r")
    if f:
      temp = float(f.read()) / 1000.0

  return temp

def get_os():
  p = pathlib.Path("/etc/os-release")

  os = "unknown"
  if p.exists():
    os = "Debian"

  return os

info = {
  # Generic system info
  "sys.version": sys.version,
  "sys.version_info": sys.version_info,
  "sys.api_version": sys.api_version,
  "sys.getrecursionlimit()": sys.getrecursionlimit(),
  "sys.implementation": sys.implementation,
  "sys.int_info": sys.int_info,
  "sys.maxsize": sys.maxsize,
  "sys.platform": sys.platform,
  # "sys.stdlib_module_names": sys.stdlib_module_names,
  # "sys.modules": sys.modules,
  "sys.thread_info": sys.thread_info,

  # os
  "os.uname": os.uname(),

  # platform
  "platform.machine": platform.machine(),
  "platform.node": platform.node(),
  "platform.platform": platform.platform(),

  # Calculated info
  "os": get_os(),
  "cpu_temp": get_cpu_temp(),
  "hostname": socket.gethostname(),
}

# Dump all info
for key in info:
  print(f"{key}: {info[key]}")
